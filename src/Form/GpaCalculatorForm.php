<?php

namespace Drupal\gpa_calculator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Implements an example form.
 */
class GpaCalculatorForm extends FormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gpa_calculator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gpa_calculator.gpa');

    $gpa_instructions = $config->get('instructions') != '' ? $config->get('instructions') : '';
    $gpa_calculator_grades = $config->get('grades');
    $gpa_calculator_grades_array = [];
    // Turn grades string into array.
    if (!is_null($gpa_calculator_grades)) {
      foreach (explode("\n", $gpa_calculator_grades) as $grade) {
        [$key, $value] = explode('|', $grade, 2);
        $gpa_calculator_grades_array[$key] = $value;
      }
    }

    $form['gpa_instructions'] = [
      '#markup' => '<div class="gpa-calculator-instructions-wrapper">' . $gpa_instructions . '</div>',
    ];

    $add_row_link = \Drupal::service('link_generator')->generate($this->t('Add Row'), Url::fromRoute('<front>', $route_parameters = [], [
      'attributes' => ['id' => 'gpa-add-row'],
    ]));

    $form['add_row'] = [
      '#markup' => '<div id="gpa-add-row">' . $add_row_link . '</div>'
    ];

    $gpa_table_head = '<div id="grades_table">';
    $gpa_table_head .= '<div class="gpa-table-thead">';
    $gpa_table_head .= '<div class="gpa-table-cell gpa-th">#</div>';
    $gpa_table_head .= '<div class="gpa-table-cell gpa-th">' . $this->t('Class/Course Name') . '</div>';
    $gpa_table_head .= '<div class="gpa-table-cell gpa-th">' . $this->t('Grade') . '</div>';
    $gpa_table_head .= '<div class="gpa-table-cell gpa-th">' . $this->t('Credits Earned') . '</div>';
    $gpa_table_head .= '</div>';
    $gpa_table_head .= '<div class="gpa-table-body"></div>';

    $form['gpa_table_head'] = [
      '#markup' => $gpa_table_head,
    ];

    $gpa_table_end = '</div>';

    $form['gpa_table_end'] = [
      '#markup' => $gpa_table_end,
    ];

    $form['prev_gpa'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cumulative GPA'),
      '#attributes' => ['id' => 'prev-gpa'],
      '#size' => 3,
    ];

    $form['prev_hours'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cumulative Credits Earned'),
      '#attributes' => ['id' => 'prev-hours'],
      '#size' => 3,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate'),
      '#button_type' => 'primary',
    ];

    $form['gpa_results_wrapper'] = [
      '#markup' => '<div class="gpa-results-wrapper">',
    ];

    $form['gpa_current_output'] = [
      '#markup' => '<div id="gpa-current-output"></div>',
    ];

    $form['gpa_cumulative_output'] = [
      '#markup' => '<div id="gpa-cumulative-output"></div>',
    ];

    $form['gpa_results_wrapper_end'] = [
      '#markup' => '</div>',
    ];

    $form['#attached']['library'][] = 'gpa_calculator/gpa.calculator.form';
    $form['#attached']['library'][] = 'gpa_calculator/gpa.calculator.admin';

    $form['#attached']['drupalSettings']['gpa_calculator']['gpaCalculator']['grades'] = $gpa_calculator_grades_array;

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}
